import React, { useState } from "react";
import '../App.css';

function App() {
  const [whoIsSubscribing, setWhoIsSubscribing] = useState('buyer')

  return (
    <div >
      <h2 className='i-am-a'>I am a...</h2>
      <div>
        <input
          type='radio'
          onChange={e => setWhoIsSubscribing('buyer')}
          name='whoIsSubscribing'
          defaultChecked
        />
        <label>Buyer</label>

        <input
          type='radio'
          onChange={e => setWhoIsSubscribing('gallery')}
          name='whoIsSubscribing'
        />
        <label>Gallery</label>
      </div>

      {whoIsSubscribing === 'buyer'
        ? <form className='subscribe-form'>
          <div className='subscribe-name-wrapper'>
            <input name='firstName' placeholder='First name' />
            <input name='lastName' placeholder='Last name' />
            <input className='subscribe-email' name='email' placeholder='Your email' />
            <button className='subscribe-button'>Subscribe</button>
          </div>
        </form>
        : whoIsSubscribing === 'gallery'
          && <form className='subscribe-form'>
            <div className='subscribe-gallery-name-wrapper'>
              <input name='galleryName' placeholder='Gallery name' />
              <input className='subscribe-email' name='email' placeholder='Your email' />
              <button className='subscribe-button'>Subscribe</button>
            </div>
          </form>}

      
    </div>
  );
}

export default App;

import React from 'react'
import '../App.css'

const Home = () => {

  return (
    <div className='grid-two-columns section' id='home'>
      
      <div className='home-left'>
        <h1 className='home-title'>A Digital Space Built Just For You. Make Art Accessible.</h1>
        <p className='home-description'>Join our unique Digital Marketplace for non-fungible tokens (NFTs) to create your own private rooms and exhibit exclusive digital assets.</p>
        <button className='get-started-button'>Get started</button>
      </div>

      <div>
        <img src='https://picsum.photos/288/376' className='home-image' style={{marginRight: 24}}/>
        <img src='https://picsum.photos/288/376' className='home-image'/>       
      </div>

    </div>
    
  )
}

export default Home

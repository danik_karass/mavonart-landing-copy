import React from 'react'
import '../App.css';
import * as Scroll from 'react-scroll'

const Header = () => {

  const Link = Scroll.Link

  return (
    <div className='header'>
      <nav role="navigation">
        <div id="menuToggle">
          <input type="checkbox" />
          <span></span>
          <span></span>
          <ul id="menu">

            <li>
              <Link activeClass="active"
                to="about"
                smooth={true}
                hashSpy={true}
                duration={500}
                isDynamic={true}
                ignoreCancelEvents={false}
                spyThrottle={500}
              >
                About Us
              </Link>
            </li>

            <li>
              <Link activeClass="active"
                to="mission"
                smooth={true}
                hashSpy={true}
                duration={500}
                isDynamic={true}
                ignoreCancelEvents={false}
                spyThrottle={500}
              >
                Our Mission
              </Link>
            </li>

            <li>
              <Link activeClass="active"
                to="subscribe"
                smooth={true}
                hashSpy={true}
                duration={500}
                isDynamic={true}
                ignoreCancelEvents={false}
                spyThrottle={500}
              >
                Subscribe
              </Link>
            </li>



          </ul>
        </div>
      </nav>


      <Link activeClass="active"
        to="home"
        smooth={true}
        hashSpy={true}
        duration={500}
        isDynamic={true}
        ignoreCancelEvents={false}
        spyThrottle={500}
      >
        <img src='images/transparentLogo.png' alt='mavon logo' className='logo'></img>
      </Link>

      <button className='contact-us-button'>contact&nbsp;us</button>

    </div>
  )
}

export default Header